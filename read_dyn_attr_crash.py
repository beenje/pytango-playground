import argparse
import asyncio
import queue
import threading
import time
import numpy as np
import tango
from tango import (
    GreenMode,
    AttrQuality,
    AttrWriteType,
    DevState,
    DebugIt,
    EnsureOmniThread,
)
from tango.server import Device, attribute, command
from tango.test_context import DeviceTestContext


EVENT_PUBLISHER_ERROR_BACKOFF_TIME_SEC = 1.0
DYN_ATTRIBUTES = ["H22", "H2O18", "CO28", "O32", "CO244"]


class EnsureOmniThreadEventPushingDevice(Device):
    """Tango Device that redirects push_change_event to a queue for publishing.

    The publishing happens from a dedicated thread that runs in an
    EnsureOmniThread context.  This allows us to safely push change events.
    Pushing events from a general Python thread that is not marked as an
    OmniThread (threads used by cppTango) can lead to problems.
    E.g., Not able to acquire serialization (dev, class or process) monitor
    """

    def __init__(self, *args, **kwargs):
        self._event_queue = queue.Queue()
        self._publisher = threading.Thread(
            target=self._publisher_thread, name="publisher"
        )
        self._publisher.daemon = True
        self._publisher.start()
        super().__init__(*args, **kwargs)

    def push_change_event(self, attr_name, data):
        self._event_queue.put((attr_name, data))

    def _publisher_thread(self):
        with EnsureOmniThread():
            attr_name = "unset"
            data = None
            while True:
                try:
                    attr_name, data = self._event_queue.get()
                    super().push_change_event(attr_name, data)
                except Exception as exc:
                    self.error_stream(
                        f"General exception in publisher thread for attr {attr_name}, "
                        f"value {data}: {exc}"
                    )
                    time.sleep(EVENT_PUBLISHER_ERROR_BACKOFF_TIME_SEC)


class MyDevice(EnsureOmniThreadEventPushingDevice):
    green_mode = GreenMode.Asyncio

    LastScanData = attribute(
        dtype=(float,),
        max_dim_x=1024,
        access=AttrWriteType.READ,
        doc="Last Scan Data. Return last data acquired in the current scan.",
    )

    dynamicAttributesList = attribute(
        dtype=(str,),
        max_dim_x=64,
        access=AttrWriteType.READ,
        doc="Returns list of all dynamic attributes",
    )

    @DebugIt()
    async def init_device(self):
        await super().init_device()
        self.set_state(DevState.INIT)
        self._last_data = []
        self._dynamic_attributes = []
        self.mas = MyDeviceIO(
            data_callback=self._update_data,
        )
        self.set_state(DevState.ON)

    @DebugIt()
    async def delete_device(self):
        try:
            await self.mas.stop()
        except Exception:
            pass
        await super().delete_device()

    @DebugIt(show_args=True)
    def _update_data(self, data):
        self._last_data = data
        for index, attr_name in enumerate(self._dynamic_attributes):
            self.push_change_event(attr_name, self._last_data[index])

    @DebugIt()
    def read_LastScanData(self):
        return self._last_data

    @DebugIt()
    def read_dynamicAttributesList(self):
        return self._dynamic_attributes

    @DebugIt()
    @command
    async def Start(self):
        self._last_data = []
        self._generate_dynamic_attributes()
        await self.mas.start()
        self.set_state(DevState.RUNNING)

    @DebugIt()
    @command
    async def Stop(self):
        await self.mas.stop()
        self.set_state(DevState.ON)

    def _generate_dynamic_attributes(self):
        # Remove existing dynamic attributes first
        for attr_name in self._dynamic_attributes:
            self.remove_attribute(attr_name)

        self._dynamic_attributes = []

        for legend in DYN_ATTRIBUTES:
            attr_name = legend.replace(" ", "").replace(":", "")
            self._dynamic_attributes.append(attr_name)
            attr = attribute(
                name=attr_name,
                dtype=float,
                access=AttrWriteType.READ,
                fget=self._read_dynamic_attribute,
            )
            self.info_stream(f"Adding dynamic attribute {attr_name}")
            self.add_attribute(attr)
            self.set_change_event(attr_name, True, False)

    @DebugIt(show_args=True)
    def _read_dynamic_attribute(self, attr):
        attr_name = attr.get_name()
        attr_index = self._dynamic_attributes.index(attr_name)
        try:
            value = self._last_data[attr_index]
        except IndexError:
            attr.set_value_date_quality(0, time.time(), AttrQuality.ATTR_INVALID)
        else:
            attr.set_value(value)


class MyDeviceIO:
    def __init__(
        self,
        data_callback=None,
    ):
        self.task = None
        self.data_callback = data_callback

    async def start(self):
        self.task = asyncio.create_task(self._generate(self.data_callback))

    async def stop(self):
        if self.task is None:
            return
        self.task.cancel()
        await asyncio.gather(self.task, return_exceptions=True)
        self.task = None

    async def _generate(self, callback):
        """Background task to generate data"""
        while True:
            data = np.random.rand(10)
            await run_callback(callback, data)
            await asyncio.sleep(0.5)


async def run_callback(func, *args):
    if func is None:
        return
    if asyncio.iscoroutinefunction(func):
        result = await func(*args)
    else:
        loop = asyncio.get_running_loop()
        result = await loop.run_in_executor(None, func, *args)
    return result


def run_server(parsed_args, *args):
    MyDevice.run_server([parsed_args.instance] + list(args))


def run_client(parsed_args, *args):
    proxy = tango.DeviceProxy(parsed_args.device)
    start_and_read(proxy, parsed_args.sleep)


def run_test(parsed_args, *args):
    with DeviceTestContext(MyDevice, process=True) as proxy:
        start_and_read(proxy, parsed_args.sleep)


def start_and_read(proxy, sleep):
    proxy.Start()
    while True:
        attributes = proxy.read_attributes(DYN_ATTRIBUTES)
        for attr in attributes:
            print(f"Reading {attr.name}: {attr.value}")
        time.sleep(sleep)


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(required=True, help="valid subcommands")
    # server subcommand
    parser_server = subparsers.add_parser("server", help="run the server")
    parser_server.add_argument("instance", help="Tango instance name")
    parser_server.set_defaults(func=run_server)
    # client subcommand
    parser_client = subparsers.add_parser("client", help="run the client")
    parser_client.add_argument("device", help="Tango device name")
    parser_client.add_argument(
        "--sleep",
        type=float,
        help="sleep between read in seconds [default: 2.0]",
        default=2.0,
    )
    parser_client.set_defaults(func=run_client)
    # test subcommand
    parser_test = subparsers.add_parser(
        "test", help="run both the server and client (using DeviceTestContext)"
    )
    parser_test.add_argument(
        "--sleep",
        type=float,
        help="sleep between read in seconds [default: 2.0]",
        default=2.0,
    )
    parser_test.set_defaults(func=run_test)
    args, unknown = parser.parse_known_args()
    return (args, unknown)


def main():
    args, unknown = parse_args()
    args.func(args, *unknown)


if __name__ == "__main__":
    main()
